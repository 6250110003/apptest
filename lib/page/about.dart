import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              width: 400.0,
              height: 250.0,
              alignment: Alignment.center,
              child: Image.asset('assets/images/6250110003.jpg'),
            ),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นางสาวนภัสสร บูก้ง 6250110003 ",
              style: GoogleFonts.kodchasan(fontSize: 15),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              width: 400.0,
              height: 250.0,
              alignment: Alignment.center,
              child: Image.asset('assets/images/6250110008.jpg'),
            ),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นางสาวมัณฑิตา ระเบียบดี 6250110008 ",
              style: GoogleFonts.kodchasan(fontSize: 15),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              width: 400.0,
              height: 250.0,
              alignment: Alignment.center,
              child: Image.asset('assets/images/6250110014.jpg'),
            ),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นายสาคเรศ นาคประสิทธิ์ 6250110014 ",
              style: GoogleFonts.kodchasan(fontSize: 15),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              width: 400.0,
              height: 250.0,
              alignment: Alignment.center,
              child: Image.asset('assets/images/6250110026.jpg'),
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: name(
              text: Text(
                "นางสาวนุชนาถ เหมาะเเหล่ 6250110026 ",
                style: GoogleFonts.kodchasan(fontSize: 15),
              ),
            ),
          ),
        ],
      ),
    )));
  }
}

class name extends StatelessWidget {
  const name({Key key, this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        color: Colors.amber,
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 30),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}
